package main.java;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TicketZero {

	@RequestMapping(
			"/"
			)
	public String tickZero() {
		return getClass().getSimpleName();
	}
}
